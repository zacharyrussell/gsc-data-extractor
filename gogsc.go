package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"google.golang.org/api/option"
	"google.golang.org/api/webmasters/v3"

	"golang.org/x/oauth2"

	"cloud.google.com/go/storage"
	"golang.org/x/oauth2/google"
)

func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func main() {

	context := context.Background()
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}
	config, err := google.ConfigFromJSON(b, webmasters.WebmastersScope, storage.ScopeReadWrite)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	getClient(config)
	tok, err := tokenFromFile("token.json")
	webmastersService, err := webmasters.NewService(context, option.WithTokenSource(config.TokenSource(context, tok)))
	if err != nil {
		log.Fatal(err)
	}

	var req = webmasters.SearchAnalyticsQueryRequest{
		EndDate:    "2019-06-02",
		StartDate:  "2019-06-01",
		Dimensions: []string{"page", "query"},
		RowLimit:   25000,
		// StartRow:   40001,
	}
	type dataRow struct {
		Url         string
		Clicks      float64
		Ctr         float64
		Position    float64
		Keyword     string
		Impressions float64
	}
	data := webmastersService.Searchanalytics.Query("https:www.underarmour.com", &req)
	res, err := data.Do()
	// fullres, _ := json.Marshal(res.Rows)
	var cleanData []dataRow
	for _, key := range res.Rows {
		cleanData = append(cleanData, dataRow{key.Keys[0], key.Clicks, key.Ctr, key.Position, key.Keys[1], key.Impressions})
	}

	type dataExport struct {
		Data []dataRow
	}
	var buffer bytes.Buffer
	for _, record := range cleanData {
		body, _ := json.Marshal(record)
		buffer.Write(body)
		buffer.WriteString("\n")
	}
	// cleanJSON, _ := json.Marshal(cleanData)
	// formatted, _ := json.Marshaler(fullres)
	// // fmt.Printf("%T\n", res)
	// fmt.Println(string(fullres))

	// projectID := "gsc-importer-242717"

	// Creates a client.
	client, err := storage.NewClient(context)

	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Sets the name for the new bucket.
	bucketName := "thisismybucketnamedoyoulikeit"

	// Creates a Bucket instance.
	bucket := client.Bucket(bucketName)

	// // Creates the new bucket.
	// if err := bucket.Create(context, projectID, nil); err != nil {
	// 	log.Fatalf("Failed to create bucket: %v", err)
	// }

	// fmt.Printf("Bucket %v created.\n", bucketName)

	f, err := os.Open("token.json")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	wc := bucket.Object("data599.json").NewWriter(context)
	// if _, err = io.Copy(wc, f); err != nil {
	if _, err = wc.Write(buffer.Bytes()); err != nil {
		log.Fatal(err)
	}
	if err := wc.Close(); err != nil {
		log.Fatal(err)
	}
}
